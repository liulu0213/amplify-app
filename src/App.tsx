import './App.css';
import {createTodo, deleteTodo} from './graphql/mutations'
import {listTodos} from './graphql/queries'
import {withAuthenticator, Button, Text, Flex, Heading} from "@aws-amplify/ui-react";
import {useCallback, useEffect, useState} from 'react';
import {API} from 'aws-amplify';

function App({signOut}) {
  const [todos, setTodos] = useState([])

  const fetchNotes = useCallback(async () => {
    const result = await API.graphql({
      query: listTodos,
      authMode: 'AMAZON_COGNITO_USER_POOLS'
    })
    setTodos(result.data.listTodos.items)
  }, [setTodos])

  const handleCreateNote = useCallback(async () => {
    await API.graphql({
      query: createTodo,
      variables: {
        fields: {name: window.prompt("New Todo"), description: window.prompt("Description")}
      },
      authMode: 'AMAZON_COGNITO_USER_POOLS'
    })
    fetchNotes()
  }, [fetchNotes])

  const handleDeleteNote = useCallback(async (id) => {
    await API.graphql({
      query: deleteTodo,
      variables: {input: {id: id}},
      authMode: 'AMAZON_COGNITO_USER_POOLS'
    })
    fetchNotes()
  }, [fetchNotes])

  useEffect(() => {
    fetchNotes()
  }, [fetchNotes])

  return (
    <Flex direction={"column"}>
      <Flex justifyContent={'space-between'}>
        <Heading level={1}>My Todos</Heading>
        <Button onClick={signOut}>Sign Out</Button>
      </Flex>
      {todos.map(todo => <Flex alignItems={'center'}>
        <Text>{`${todo.name}:${todo.description}`}</Text>
        <Button onClick={() => handleDeleteNote(todo.id)}>Remove</Button>
      </Flex>)}
      <Button onClick={handleCreateNote}>Add Todo</Button>
    </Flex>
  );
}

export default withAuthenticator(App);
